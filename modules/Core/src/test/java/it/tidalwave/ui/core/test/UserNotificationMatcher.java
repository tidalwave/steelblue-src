/*
 * *************************************************************************************************************************************************************
 *
 * SteelBlue: DCI User Interfaces
 * http://tidalwave.it/projects/steelblue
 *
 * Copyright (C) 2015 - 2025 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *************************************************************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
 *
 * *************************************************************************************************************************************************************
 *
 * git clone https://bitbucket.org/tidalwave/steelblue-src
 * git clone https://github.com/tidalwave-it/steelblue-src
 *
 * *************************************************************************************************************************************************************
 */
package it.tidalwave.ui.core.test;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import java.util.regex.Pattern;
import it.tidalwave.ui.core.UserNotification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.mockito.ArgumentMatcher;
import static lombok.AccessLevel.PACKAGE;

/***************************************************************************************************************************************************************
 *
 * @stereotype  mockito matcher
 *
 * @since   2.0-ALPHA-2
 * @author  Fabrizio Giudici
 *
 **************************************************************************************************************************************************************/
@Slf4j @RequiredArgsConstructor(access = PACKAGE)
public class UserNotificationMatcher implements ArgumentMatcher<UserNotification>
  {
    @Nonnull
    private final String captionRegex;

    @Nonnull
    private final String textRegex;

    /***********************************************************************************************************************************************************
     * {@inheritDoc}
     **********************************************************************************************************************************************************/
    @Override
    public boolean matches (@Nullable final UserNotification notification)
      {
        return (notification != null)
                && Pattern.matches(captionRegex, notification.getCaption())
                && Pattern.matches(textRegex, notification.getText());
      }

    /***********************************************************************************************************************************************************
     * {@inheritDoc}
     **********************************************************************************************************************************************************/
    @Override @Nonnull
    public String toString()
      {
        return String.format(
                "UserNotification(text matching %s, caption matching %s, feedback doesn't matter)",
                textRegex, captionRegex);
      }
  }

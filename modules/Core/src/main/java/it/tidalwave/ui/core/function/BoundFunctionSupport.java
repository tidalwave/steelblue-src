/*
 * *************************************************************************************************************************************************************
 *
 * SteelBlue: DCI User Interfaces
 * http://tidalwave.it/projects/steelblue
 *
 * Copyright (C) 2015 - 2025 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *************************************************************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
 *
 * *************************************************************************************************************************************************************
 *
 * git clone https://bitbucket.org/tidalwave/steelblue-src
 * git clone https://github.com/tidalwave-it/steelblue-src
 *
 * *************************************************************************************************************************************************************
 */
package it.tidalwave.ui.core.function;

import java.beans.PropertyChangeSupport;
import it.tidalwave.ui.core.role.impl.MutableListenerSupport;
import lombok.experimental.Delegate;

/***************************************************************************************************************************************************************
 *
 * @since   2.0-ALPHA-1
 * @author  Fabrizio Giudici
 *
 **************************************************************************************************************************************************************/
@SuppressWarnings("this-escape")
public abstract class BoundFunctionSupport<T, R> implements BoundFunction<T, R>
  {
    @Delegate // FIXME: weak
    private final PropertyChangeSupport pcs = new PropertyChangeSupport(this);

    @Delegate
    private final MutableListenerSupport mls = new MutableListenerSupport(pcs);

    /***********************************************************************************************************************************************************
     * {@inheritDoc}
     **********************************************************************************************************************************************************/
    @Override
    public void unbindAll()
      {
        for (final var listener : pcs.getPropertyChangeListeners().clone())
          {
            pcs.removePropertyChangeListener(listener);
          }
      }

    protected void fireValueChanged (final R oldValue, final R newValue)
      {
        pcs.firePropertyChange("value", oldValue, newValue);
      }

    protected void fireValueChanged (final boolean oldValue, final boolean newValue)
      {
        pcs.firePropertyChange("value", oldValue, newValue);
      }
  }

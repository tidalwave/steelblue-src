/*
 * *************************************************************************************************************************************************************
 *
 * SteelBlue: DCI User Interfaces
 * http://tidalwave.it/projects/steelblue
 *
 * Copyright (C) 2015 - 2025 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *************************************************************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
 *
 * *************************************************************************************************************************************************************
 *
 * git clone https://bitbucket.org/tidalwave/steelblue-src
 * git clone https://github.com/tidalwave-it/steelblue-src
 *
 * *************************************************************************************************************************************************************
 */
package it.tidalwave.ui.core.function;

import jakarta.annotation.Nonnull;
import it.tidalwave.ui.core.ChangingSource;

/***************************************************************************************************************************************************************
 *
 * @since   2.0-ALPHA-1
 * @author  Fabrizio Giudici
 *
 **************************************************************************************************************************************************************/
@SuppressWarnings("this-escape")
public abstract class UnaryBoundFunctionSupport<T, R> extends BoundFunctionSupport<T, R>
  {
    @Nonnull
    private final ChangingSource<T> source;

    protected R value;

    @SuppressWarnings("unchecked")
    protected UnaryBoundFunctionSupport (@Nonnull final ChangingSource<T> source)
      {
        this.source = source;
        source.addPropertyChangeListener(event -> onSourceChange((T)event.getOldValue(), (T)event.getNewValue()));
      }

    protected void onSourceChange (@Nonnull final T oldSourceValue, @Nonnull final T newSourceValue)
      {
        final var oldValue = function(oldSourceValue);
        value = function(newSourceValue);
        fireValueChanged(oldValue, value);
      }

    @Nonnull
    protected abstract R function (final T value);

    @Override @Nonnull
    public final R get()
      {
        return value;
      }
  }

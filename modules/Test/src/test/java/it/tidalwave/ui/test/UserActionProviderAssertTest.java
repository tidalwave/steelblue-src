/*
 * *************************************************************************************************************************************************************
 *
 * SteelBlue: DCI User Interfaces
 * http://tidalwave.it/projects/steelblue
 *
 * Copyright (C) 2015 - 2025 by Tidalwave s.a.s. (http://tidalwave.it)
 *
 * *************************************************************************************************************************************************************
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 * CONDITIONS OF ANY KIND, either express or implied.  See the License for the specific language governing permissions and limitations under the License.
 *
 * *************************************************************************************************************************************************************
 *
 * git clone https://bitbucket.org/tidalwave/steelblue-src
 * git clone https://github.com/tidalwave-it/steelblue-src
 *
 * *************************************************************************************************************************************************************
 */
package it.tidalwave.ui.test;

import it.tidalwave.ui.core.role.UserAction;
import it.tidalwave.ui.core.role.UserActionProvider;
import it.tidalwave.util.Callback;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

/***************************************************************************************************************************************************************
 *
 * @author  Fabrizio Giudici
 *
 **************************************************************************************************************************************************************/
public class UserActionProviderAssertTest
  {
    private UserActionProviderAssert underTest;

    private UserAction userAction1;

    private UserAction userAction2;

    private UserAction userAction3;

    /**********************************************************************************************************************************************************/
    @BeforeMethod
    public void setup()
      {
        userAction1 = UserAction.of(mock(Callback.class));
        userAction2 = UserAction.of(mock(Callback.class));
        userAction3 = UserAction.of(mock(Callback.class));
        final var userActionProvider = UserActionProvider.of(userAction1, userAction2, userAction3);
        underTest = UserActionProviderAssert.of(userActionProvider);
      }

    /**********************************************************************************************************************************************************/
    @Test
    public void testGood()
      {
        // when
        final var enumerator = underTest.withActionCount(3);
        // then
        assertThat(enumerator.itemType).isEqualTo(UserAction.class);
        assertThat(enumerator.items).containsExactly(userAction1, userAction2, userAction3);
      }

    /**********************************************************************************************************************************************************/
    @Test(expectedExceptions = AssertionError.class)
    public void testBad()
      {
        // when
        underTest.withActionCount(2);
        // then assertion failed
      }
  }